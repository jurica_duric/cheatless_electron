const {
    app,
    BrowserWindow,
    ipcMain,
    shell,
    session
  } = require("electron");
  const path = require("path");
  const fs = require("fs");
const { windowsStore } = require("process");
  
  // Keep a global reference of the window object, if you don't, the window will
  // be closed automatically when the JavaScript object is garbage collected.
  let win;
  
  async function createWindow() {
  
    // Create the browser window.
    win = new BrowserWindow({
      width: 800,
      height: 600,
      kiosk: true,
      //frame: false,
      //alwaysOnTop: true,
      webPreferences: {
        nodeIntegration: false, // is default value after Electron v5
        contextIsolation: true, // protect against prototype pollution
        enableRemoteModule: false, // turn off remote
        preload: path.join(__dirname, "preload.js") // use a preload script
      }
    });
  
    // Load app
    win.loadFile(path.join(__dirname, "index.html"));
    //win.loadFile(path.join(__dirname, "loader.html"));
    // rest of code..
    //session.defaultSession.on('will-download', (event, item, webContents) => {
     console.log('download started');
     
    win.webContents.session.on('will-download', (event, item, webContents) => {
      //event.preventDefault()
      let name = item.getFilename();
     /*
      require('request')(item.getURL(), (data) => {
        console.log(item.getURL())
        console.log(data);
        //require('fs').writeFileSync('/video', data)
      })*/
      // Set the save path, making Electron not to prompt a save dialog.
      //item.setSavePath('C:\\Data\\video\\save.webm')// get the filename from item object and provide here according to your loic
      item.setSavePath(path.join(__dirname, "video", name));
    item.on('updated', (event, state) => {
      if (state === 'interrupted') {
        console.log('Download is interrupted but can be resumed')
      } else if (state === 'progressing') {
        if (item.isPaused()) {
          console.log('Download is paused')
        } else {
          console.log(`Received bytes: ${item.getReceivedBytes()}`)
        }
      }
    })
    item.once('done', (event, state) => {
      if (state === 'completed') {
        console.log('Download successfully')
        //Open the document using the external application
        //shell.openItem(item.getSavePath());
      } else {
        console.log(`Download failed: ${state}`)
      }
    })
  });
  }
  app.commandLine.appendSwitch('ignore-certificate-errors');
  app.on("ready", createWindow);
  
  ipcMain.on('close',(event,data)=>{
    app.quit()
  });

 
  ipcMain.on("toMain", (event, args) => {
    win.webContents.send("fromMain", "gjhfhgghd");
    console.log(args);
    /*fs.readFile("style.css", (error, data) => {
      // Do something with file contents
  
      // Send result back to renderer process
      win.webContents.send("fromMain", "gjhfhgghd");
    });*/
  });