/*
*  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
*
*  Use of this source code is governed by a BSD-style license
*  that can be found in the LICENSE file in the root of the source
*  tree.
*/

// This code is adapted from
// https://rawgit.com/Miguelao/demos/master/mediarecorder.html

'use strict';

/* globals MediaRecorder */

let mediaRecorder;
let recordedBlobs;

const errorMsgElement = document.querySelector('span#errorMsg');
const recordedVideo = document.querySelector('video#recorded');
const recordButton = document.querySelector('button#record');
recordButton.addEventListener('click', () => {
  if (recordButton.textContent === 'Start Recording') {
    startRecording();
  } else {
    stopRecording();
  }
});


function download() {
	stopRecording();
	setTimeout(() => {
    document.getElementById("download").click();
  }, 100);
  clearInterval(timer);
}

const downloadButton = document.querySelector('button#download');
downloadButton.addEventListener('click', () => {
  /*const blob = new Blob(recordedBlobs, {type: 'video/x-matroska;codecs=avc1});*/
  const blob = new Blob(recordedBlobs, {type: 'video/webm;codecs=vp8'});
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.style.display = 'none';
  var d = new Date();
  var df=d.getDate() + "_" + (d.getMonth()+1) + "_" + d.getFullYear() + "_"+ d.getHours() + "_" + d.getMinutes() + "_" + d.getSeconds();
  a.href = url;
  /*a.download = 'test.mp4';*/
  //a.download = 'test.webm';
  a.download = df;
  document.body.appendChild(a);
  a.click();
  setTimeout(() => {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 100);
});

function handleDataAvailable(event) {
  console.log('handleDataAvailable', event);
  if (event.data && event.data.size > 0) {
    recordedBlobs.push(event.data);
  }
}

function startRecording() {
	myFunction();
  recordedBlobs = [];
  let options = {mimeType: 'video/webm;codecs=vp9,opus'};
  if (!MediaRecorder.isTypeSupported(options.mimeType)) {
    console.error('${options.mimeType} is not supported');
    options = {mimeType: 'video/webm;codecs=vp8,opus'};
    if (!MediaRecorder.isTypeSupported(options.mimeType)) {
      console.error('${options.mimeType} is not supported');
      options = {mimeType: 'video/webm'};
      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        console.error('${options.mimeType} is not supported');
        options = {mimeType: ''};
      }
    }
  }

  try {
    mediaRecorder = new MediaRecorder(window.stream, options);
  } catch (e) {
    console.error('Exception while creating MediaRecorder:', e);
    errorMsgElement.innerHTML = 'Exception while creating MediaRecorder: ${JSON.stringify(e)}';
    return;
  }

  console.log('Created MediaRecorder', mediaRecorder, 'with options', options);
  recordButton.textContent = 'Stop Recording';
  //downloadButton.disabled = true;
  mediaRecorder.onstop = (event) => {
    console.log('Recorder stopped: ', event);
    console.log('Recorded Blobs: ', recordedBlobs);
  };
  mediaRecorder.ondataavailable = handleDataAvailable;
  mediaRecorder.start();
  console.log('MediaRecorder started', mediaRecorder);
}

function stopRecording() {
  mediaRecorder.stop();
  recordButton.textContent = 'Start Recording';
  downloadButton.disabled = false;
}

function handleSuccess(stream) {
  recordButton.disabled = false;
  console.log('getUserMedia() got stream:', stream);
  window.stream = stream;

  const gumVideo = document.querySelector('video#gum');
  gumVideo.srcObject = stream;
}

async function init(constraints) {
  try {
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    handleSuccess(stream);
  } catch (e) {
    console.error('navigator.getUserMedia error:', e);
    errorMsgElement.innerHTML = 'navigator.getUserMedia error:${e.toString()}';
  }
}

document.querySelector('button#start').addEventListener('click', async () => {
  //const hasEchoCancellation = document.querySelector('#echoCancellation').checked;
  const constraints = {
    audio: {
      echoCancellation: {exact: false}
    },
    video: {
      //width: 1280, height: 720
	  //width:240, height: 160
      width:640, height: 480
    }
  };
  console.log('Using media constraints:', constraints);
  await init(constraints);
  document.getElementById("record").click();
});
var timer 

function myFunction() {
  timer = setInterval(download, 30 * 1000);
}
function doSomething() {
	stopRecording();
	const blob = new Blob(recordedBlobs, {type: 'video/webm;codecs=vp8'});
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.style.display = 'none';
  a.href = url;
  /*a.download = 'test.mp4';*/
  a.download = 'test.webm';
  document.body.appendChild(a);
  a.click();
  setTimeout(() => {
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }, 100);
	//alert("Nakon 10 sekundi!");
	clearInterval(timer);
} 